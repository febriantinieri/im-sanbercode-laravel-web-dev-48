<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function formulir()
    {
        return view('page.form');
    }

    public function hallo(Request $request)
    {
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');

        return view('page.hallo', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}