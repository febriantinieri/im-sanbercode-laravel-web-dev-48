<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        //untuk ngecek data terkirim
        //dd($request->all());

        //validasi
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //insert data ke database
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        //arahkan ke halaman /cast
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();

        //untuk ngecek data terkirim
        //dd($cast);

        return view('cast.tampil', ['cast' => $cast]);

    }

    public function show($id)
    {
        $castData = DB::table('cast')->find($id);
        return view('cast.detail', ['castData' => $castData]);

    }

    public function edit($id)
    {
        $castData = DB::table('cast')->find($id);
        return view('cast.edit', ['castData' => $castData]);

    }

    public function update($id, Request $request)
    {
        //validasi
        $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        //update data 
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio')
            ]);

        //Redirect halaman
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}