@extends('layouts.master')
@section('judul')
    Tambah Cast
@endsection

@section('content')
<form method="post" action="/cast">
    @csrf
    <div class="form-group">
      <label>Nama Artis</label>
      <input type="text" name="nama" class="form-control @error('nama') is invalid @enderror">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Umur</label>
      <input type="numeric" name="umur" class="form-control @error('umur') is invalid @enderror">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Biografi</label>
        <textarea name="bio" id="" cols="30" rows="5" class="form-control @error('bio') is invalid @enderror"></textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection