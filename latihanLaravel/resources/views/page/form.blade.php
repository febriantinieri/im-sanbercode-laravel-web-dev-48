@extends('layouts.master')
@section('judul')
    Buat Account Baru ! 
@endsection

@section('content')
<form action="/hallo" method="post">
    @csrf
    First Name: <br>
    <input type="text" name="fname"> <br><br>
    Last Name:<br>
    <input type="text" name="lname"><br><br>
    Gender:<br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>
    Nationality:<br>
    <select name="nasional" id="">
        <option value="Indonesian">Indonesian</option>
        <option value="Italian">Italian</option>
        <option value="Malaysian">Malaysian</option>
    </select><br><br>
    Language Spoken:<br>
    <input type="checkbox" name="indo" id="">Bahasa Indonesian<br>
    <input type="checkbox" name="english" id="">English<br>
    <input type="checkbox" name="other" id="">Other<br><br>
    Bio:<br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up" name="signup"><br>
</form>

@endsection
