<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

// use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/form', [AuthController::class, 'formulir']);
Route::post('/hallo', [AuthController::class, 'hallo']);

Route::get('/data-table', function () {
    return view('page.data-table');
});

Route::get('/table', function () {
    return view('page.table');
});

//CRUD table cast

//C => Create Data
// Route yang mengarah ke halaman tambah data
Route::get('/cast/create', [CastController::class, 'create']);
//Route untuk menyimpan data inputan ke database
Route::post('/cast', [CastController::class, 'store']);

//R =? Read Data
//Route yang menampilkan semua data di database ke blade web browsernya
Route::get('/cast', [CastController::class, 'index']);
//Route detail data berdasarkan id 
Route::get('/cast/{id}', [CastController::class, 'show']);

//U => Update Data
//Route untuk mengarah ke halaman update data berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Route untuk update data ke database berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

//D => Delete
//Route untuk mendelete berdasarkan id
Route::delete('/cast/{id}', [castController::class, 'destroy']);